import os
from fabric.api import run, env, settings, hide, cd, sudo, put

project_name = 'flask-imageboard-master'


env.shell = 'sh -c'
env.hosts = ['1.2.3.4']
env.user = 'root'
env.password = 'password'


with open('nginx_file/nginx_file_base', 'r') as nginx_f:
    param_read = nginx_f.read()
    param_write = param_read.replace('REPLACE_ME', project_name)
    with open('nginx_file/flask_project', 'w') as new_nginx_f:
        new_nginx_f.write(param_write)


def install_flask():
    with settings(hide('stdout'), warn_only=True):
        run('apt-get update')
        run('apt-get upgrade')
        run('apt-get install -y python3-pip')

        print('Python and venv install!')

        run('apt-get install -y nginx')
        sudo('mkdir /home/www')
        run('apt-get install -y python-virtualenv')
        run('virtualenv env')
        run('pip3 install gunicorn')
        run('pip3 install Flask')

        print('All libs are installed')

        put(os.getcwd()+r'\{}'.format(project_name), '/home/www', use_sudo=True)
        run('/etc/init.d/nginx start')
        sudo('rm /etc/nginx/sites-enabled/default')
        sudo('touch /etc/nginx/sites-available/flask_project')
        sudo('ln -s /etc/nginx/sites-available/flask_project /etc/nginx/sites-enabled/flask_project')
        put(os.getcwd() + r'\nginx_file\flask_project', '/etc/nginx/sites-enabled', use_sudo=True)
        sudo('/etc/init.d/nginx restart')

        print("Great! All done!")


def start_flask():
    with cd('/home/www/'+project_name):
        run('gunicorn app:app -b 0.0.0.0:8080')


def restart_flask():
    with settings(warn_only=True):
        gunicorn = run('pgrep gunicorn').replace('\r', '')
        nginx = run('pgrep nginx').replace('\r', '')

        if gunicorn != '' or nginx != '':
            gunicorn = gunicorn.split('\n')
            nginx = nginx.split('\n')
            for i in (gunicorn + nginx):
                run('kill -9 ' + str(i))
        run('/etc/init.d/nginx start')
        start_flask()
